<?php
namespace Tweeps\Interfaces;

Interface TweepsInterface
{
	
	public function extractData();
	
	public function filterMostMentions();
	
}