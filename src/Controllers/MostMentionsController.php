<?php
namespace Tweeps\Controllers;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class MostMentionsController implements ControllerProviderInterface
{
	
	public function connect(Application $app)
	{
		$service = new \Tweeps\Services\TweepsService();
		$service->extractData();
		
		$controller = $app['controllers_factory'];
		
		$controller->get('/', function(Application $app) use ($service) {
			$data = $service->filterMostMentions();
			return new JsonResponse($data, 200);
		});
		
		return $controller;

	}
	
}
