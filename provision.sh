#!/bin/bash

# update repositories for php7

add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y apache2 php7.1 php7.1-mcrypt php7.1-curl php7.1-mbstring git phpunit git-flow

# update apache config for prettly urls

sed -i 's/AllowOverride\ None/AllowOverride\ All/g' /etc/apache2/apache2.conf

# install composer

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

# configure apache to accept php7

a2dismod php5
a2enmod php7.1
a2enmod rewrite

# restart apache

service apache2 restart

exit 0
